const oldhandler = require.extensions['.ts']
const JsTypeTs ='.type.ts'
const offset = JsTypeTs.length*-1
require.extensions['.ts'] = function (m, filename) {
  if(filename.slice(offset)===JsTypeTs){
    return {}
  }else{
    return oldhandler.apply(this,arguments)
  }
}